package master.ccm2.shareatsadvert.controllers;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import master.ccm2.shareatsadvert.interfaces.ManageAdvertInterface;
import master.ccm2.shareatsadvert.messages.ErrorMessage;
import master.ccm2.shareatsadvert.messages.SuccessMessage;
import master.ccm2.shareatsadvert.models.Advert;

@RestController
public class AdvertController {

    @Autowired
    ManageAdvertInterface manageAdvertService;

    public AdvertController() {
    }

    @RequestMapping(value = "/advert", method = RequestMethod.PUT)
    public ResponseEntity<String> addAdvert(@RequestBody Advert newAdvert) {
        try {
            manageAdvertService.create(newAdvert);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return new ResponseEntity<>(ErrorMessage.SERVER_ERROR.getErrorMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(SuccessMessage.ADVERT_ADD_SUCCESS.getSuccessMessage(), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/advert/{advertId}", method = RequestMethod.GET)
    public ResponseEntity<Object> getAdvert(@PathVariable String advertId) {
        Advert advert = null;
        try {
            advert = manageAdvertService.get(advertId);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        if(advert == null) {
            return new ResponseEntity<>(ErrorMessage.NOT_FOUND.getErrorMessage(), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(advert, HttpStatus.OK);
    }

    @RequestMapping(value = "/advert/{advertId}", method = RequestMethod.POST)
    public ResponseEntity<String> editAdvert(@PathVariable String advertId, @RequestBody Advert newAdvert) {
        try {
            manageAdvertService.update(advertId, newAdvert);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return new ResponseEntity<>(ErrorMessage.SERVER_ERROR.getErrorMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(SuccessMessage.ADVERT_UPDATE_SUCCESS.getSuccessMessage(), HttpStatus.OK);
    }

    @RequestMapping(value = "/advert/{advertId}", method = RequestMethod.DELETE)
    public ResponseEntity<String> removeAdvert(@PathVariable String advertId) {
        try {
            manageAdvertService.delete(advertId);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return new ResponseEntity<>(ErrorMessage.SERVER_ERROR.getErrorMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(SuccessMessage.ADVERT_DELETE_SUCCESS.getSuccessMessage(), HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/adverts", method = RequestMethod.GET)
    public ResponseEntity<Object> getAdverts(@RequestParam double latitude, @RequestParam double longitude) {
        List<Advert> adverts = null;
        try {
            adverts = manageAdvertService.getFromLocation(latitude, longitude);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        if(adverts == null) {
            return new ResponseEntity<>(ErrorMessage.NOT_FOUND.getErrorMessage(), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(adverts, HttpStatus.OK);
    }

    @RequestMapping(value = "/adverts/{authorId}", method = RequestMethod.GET)
    public ResponseEntity<Object> getAdvertsByAuthor(@PathVariable String authorId) {
        List<Advert> adverts = null;
        try {
            adverts = manageAdvertService.getByAuthor(authorId);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        if(adverts == null) {
            return new ResponseEntity<>(ErrorMessage.NOT_FOUND.getErrorMessage(), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(adverts, HttpStatus.OK);
    }
}
