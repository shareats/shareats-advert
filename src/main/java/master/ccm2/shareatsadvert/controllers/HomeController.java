package master.ccm2.shareatsadvert.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

  @RequestMapping("/test")
  public @ResponseBody String greeting() {
    return "Hello World";
  }

}