package master.ccm2.shareatsadvert.interfaces;

import java.util.List;
import java.util.concurrent.ExecutionException;

import master.ccm2.shareatsadvert.models.Advert;

/**
 * Définition des méthodes pour créer, mettre à jour, supprimer
 */
public interface ManageAdvertInterface {

    /**
     * Ajoute une annonce à la base de données
     * @param newAdvert
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public void create(Advert newAdvert) throws InterruptedException, ExecutionException;

    /**
     * Récupère une annonce de la base de données
     * @param id Identifiant de l'annonce que l'on veut récupérer
     * @return Objet de l'annonce à afficher
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public Advert get(String id) throws InterruptedException, ExecutionException;
    
    /**
     * Modifie une annonce dans la base de données
     * @param id Identifiant de l'annonce
     * @param advert Nouvelles données de l'annonce modifiées
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public void update(String id, Advert advert) throws InterruptedException, ExecutionException;
    
    /**
     * Supprime une annonce de la base de données
     * @param id Identifiant de l'annonce
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public void delete(String id) throws InterruptedException, ExecutionException;

    /**
     * Récupère les annonces dans un rayon d'environ 10km autour des coordonnées passées
     * en paramètre
     * @param latitude Latitude des coordonnées
     * @param longitude Longitude des coordonnées
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public List<Advert> getFromLocation(double latitude, double longitude) throws InterruptedException, ExecutionException;

    /**
     * Récupère la liste des annonces publiées par l'utilisateur en paramètre
     * @param authorId ID de l'auteur
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public List<Advert> getByAuthor(String authorId) throws InterruptedException, ExecutionException;

}