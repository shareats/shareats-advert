package master.ccm2.shareatsadvert.services;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.GeoPoint;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;

import org.springframework.stereotype.Service;

import master.ccm2.shareatsadvert.interfaces.ManageAdvertInterface;
import master.ccm2.shareatsadvert.models.Advert;
import master.ccm2.shareatsadvert.models.CustomGeoPoint;

@Service
public class FirebaseAdvertService implements ManageAdvertInterface {

    private final static String ADVERT_COLLECTION = "adverts";

    private Firestore firestore;

    public FirebaseAdvertService() {
        this.initializeFirebase();
        firestore = FirestoreClient.getFirestore();
    }

    public void create(Advert newAdvert) throws InterruptedException, ExecutionException {
        this.firestore.collection(ADVERT_COLLECTION).document().set(newAdvert);
    }

    public Advert get(String id) throws InterruptedException, ExecutionException {
        DocumentReference documentReference = this.firestore.collection(ADVERT_COLLECTION).document(id);
        DocumentSnapshot document = documentReference.get().get();
        Advert advert = null;

        if(document.exists()) {
            advert = buildAdvert(document);
        }

        return advert;
    }

    public List<Advert> getFromLocation(double latitude, double longitude) throws InterruptedException, ExecutionException {
        List<Advert> results = new ArrayList<>();
        Map<String, GeoPoint> distanceValues = distanceOperations(latitude, longitude, 6.21372);
        
        ApiFuture<QuerySnapshot> future = this.firestore.collection(ADVERT_COLLECTION)
            .whereGreaterThan("location", distanceValues.get("lesserGeoPoint"))
            .whereLessThan("location", distanceValues.get("greaterGeoPoint"))
            .get();
    
        List<QueryDocumentSnapshot> documents = future.get().getDocuments();
    
        for(DocumentSnapshot document : documents) {
            results.add(buildAdvert(document));
        }
    
        return results;
    }

    @Override
    public List<Advert> getByAuthor(String authorId) throws InterruptedException, ExecutionException {
        List<Advert> results = new ArrayList<>();
        ApiFuture<QuerySnapshot> future = this.firestore.collection(ADVERT_COLLECTION).whereEqualTo("author", authorId).get();
        List<QueryDocumentSnapshot> documents = future.get().getDocuments();
    
        for(DocumentSnapshot document : documents) {
            results.add(buildAdvert(document));
        }
    
        return results;
    }

    public void update(String id, Advert advert) throws InterruptedException, ExecutionException {
        this.firestore.collection(ADVERT_COLLECTION).document(id).set(advert);
    }

    public void delete(String id) throws InterruptedException, ExecutionException {
        this.firestore.collection(ADVERT_COLLECTION).document(id).delete();
    }

    private void initializeFirebase() {

		try {
			FileInputStream serviceAccount = new FileInputStream("firebase-shareats-key.json");
			FirebaseOptions options = new FirebaseOptions.Builder()
					.setCredentials(GoogleCredentials.fromStream(serviceAccount))
					.setDatabaseUrl("https://shareats-e1dc4.firebaseio.com")
                    .build();

            if(FirebaseApp.getApps().isEmpty()) {
                FirebaseApp.initializeApp(options);
            }
        }
        catch (IOException e) {
			e.printStackTrace();
		}
    }

    private Advert buildAdvert(DocumentSnapshot document) {
        GeoPoint location = document.getGeoPoint("location");
        return new Advert(
                document.getId(),
                document.getString("title"),
                document.getString("description"),
                document.getString("author"),
                document.getDate("date"),
                new CustomGeoPoint(location.getLatitude(), location.getLongitude())
            );
    }

    private Map<String, GeoPoint> distanceOperations(double latitude, double longitude, double distance) {
        Map<String, GeoPoint> allValues = new HashMap<>();
    
        double lat = 0.0144927536231884;
        double lon = 0.018181818181818;
    
        allValues.put("lesserGeoPoint", new GeoPoint(latitude - (lat * distance), longitude - (lon * distance)));
        allValues.put("greaterGeoPoint", new GeoPoint(latitude + (lat * distance), longitude + (lon * distance)));
    
        return allValues;
    }
}
