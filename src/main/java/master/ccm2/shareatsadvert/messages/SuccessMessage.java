package master.ccm2.shareatsadvert.messages;

public enum SuccessMessage {
	
	ADVERT_ADD_SUCCESS("L'annonce a été ajoutée avec succès"),
	ADVERT_UPDATE_SUCCESS("L'annonce a été mise à jour avec succès"),
	ADVERT_DELETE_SUCCESS("L'annonce a été supprimée avec succès");
	
	private final String successMessage;

	private SuccessMessage(String message) {
		this.successMessage = message;
    }

	public String getSuccessMessage() {
		return successMessage;
	}
	
}
