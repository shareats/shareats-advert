package master.ccm2.shareatsadvert.messages;

public enum ErrorMessage {

	SERVER_ERROR("Erreur du serveur"),
	NOT_FOUND("Annonce non trouvée");

	private final String errorMessage;

	private ErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
    }

	public String getErrorMessage() {
		return errorMessage;
	}
}