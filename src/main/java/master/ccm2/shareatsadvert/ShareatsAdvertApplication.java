package master.ccm2.shareatsadvert;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShareatsAdvertApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShareatsAdvertApplication.class, args);
	}

}
