package master.ccm2.shareatsadvert.models;

import java.util.Date;

import com.google.cloud.firestore.annotation.DocumentId;

public class Advert {

    @DocumentId
    private String documentId;
    private String title, description, author;
    private Date date;
    private CustomGeoPoint location;

    public Advert() {
    }

    public Advert(String documentId, String title, String description, String author, Date date, CustomGeoPoint location) {
        this.documentId = documentId;
        this.title = title;
        this.description = description;
        this.author = author;
        this.date = date;
        this.location = location;
    }

    public String getDocumentId() {
        return this.documentId;
    }

    public String getTitle() {
        return this.title;
    }

    public String getDescription() {
        return this.description;
    }

    public String getAuthor() {
        return this.author;
    }

    public Date getDate() {
        return this.date;
    }

    public CustomGeoPoint getLocation() {
        return this.location;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    public void setLocation(CustomGeoPoint location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "{" +
            " documentId='" + getDocumentId() + "'" +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", author='" + getAuthor() + "'" +
            ", date='" + getDate() + "'" +
            ", location='" + getLocation() + "'" +
            "}";
    }

}