package master.ccm2.shareatsadvert.models;

import com.google.cloud.firestore.GeoPoint;

public class CustomGeoPoint extends GeoPoint {

    public CustomGeoPoint() {
        super(0.0, 0.0);
    }

    public CustomGeoPoint(double latitude, double longitude) {
        super(latitude, longitude);
    }
}